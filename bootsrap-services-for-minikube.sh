#!/bin/bash

#Bootstrapping commands for minikube and version 1.0.1 of the apps
#We also enable ingress (as alternative even though in beta)
# - Disadvantadge of ingress is the port 8000 not possible in short time


minikube addons enable ingress
minikube status > /dev/null
[ $? -eq 1 ]&& (echo "starting minikube" ;minikube start)

pushd deploy
kubectl apply -f countries-assembly.yml
kubectl apply -f airports-assembly.yml
kubectl apply -f ingress.yml

