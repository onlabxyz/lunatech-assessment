# Project Structure

Project is divided between folders.
Due to the time limitation I have. I decided to keep it as simple as possible.

The technologies I choose to use are:
 - Docker (for containers)
 - Kubernetes for the whole infrastructure (minikube for local use and a normal cluster for remote)


## For local use - Prerequisite

### Installation needed for:
- Docker (your favourite version)
- minikube preinstalled and enable ingress service


** I decided not to include these instructions on the document due to time limitation from my side **

### Docker images

I have create on hub.docker.com an repository `lunatechvaret` this contains the images I build based on the contents of the `containers`

| Service name | image name (under repository) | versions/tag
|:-----------|:--------:|:---------
| airports-assembly | `lunatechvaret/airports-assembly` | `1.0.1`, `1.1.0`
| countries-assembly | `lunatechvaret/countries-assembly` | `1.0.1`


### Bootstrapping a local environment

Due to time constrains from my side. I have decided/been forced to use a local minikube environment instead of a fully blown kubernetes environment

The Deploy folder creates thε following deployments

* Airports Application/service
* Countries Application/service
* Ingress service (bound by def on 80/443)

- Also tried to deploy everything with terraform (didn't gave me what I wanted and abandoned it.)


### Deploy version 1.1.0 of Airports
In order to deploy version 1.1.0 of airports you can perform this change by executing one command with kubectl

a file with the contents
```
spec:
  template:
    spec:
      containers:
      - name: airports-assembly
        image: lunatechvaret/airports-assembly:1.1.0
```

will patch the system to 1.1.0 versions after we save it on a fiel and execute the
`kubectl patch deployment airports-assembly --patch "$(cat patch-airports-assembly.yml)"`
where patch-airports-assembly.yml is the file with the content.


TO-DO
-----


1. Fix the 80 to 8000 and remove SSL redirect
2. Simplify fixing by one liner
3. Make bootstrapping shell script for non minikube setup.
