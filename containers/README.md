# Details on the Containers


In this document we will describe how the containers are build and how we can be used on a CI/CD pipeline.


## Prerequisite


In order for these images to be build we need to have the following:

- Docker CE running localy and with your user
- Access to a Docker hub to upload the images
- Setup your Docker Hub login


### Variables

#### Required Build variables to use with `--build-arg`
| Variable | Default value | Description |
|:---------|:-------------:|:------------- |
| service_sha1 | `Empty` | This variable needs to contain the sha1 of the service in order to validate the file
| service_version | `Empty` | To be used for the version
| service_name | `Empty` | This is the name of the service (fits with the prefix of the file)

#### Optional variables
| Variable | Default value | Description |
|:---------|:-------------:|:------------- |
|target_dir | `/app` | The directory inside the container to be used to store the service file
|target_file | `service.jar` | The filename of the java service to be used /stored and executed
|base_url | `https://s3-eu-west-1.amazonaws.com/devops-assesment` | The URL which the java files are stored

## How it works

The simplest way to show how this works, is by giving an example.
Based on the naming conversion it is easy to use one Dockerfile for both services.
Of-course this is only Assessment specific as the URL files stored and the logic helps us to create This

These commands can also be used to generate docker images on a CI/CD Pipeline

```
SERVICE_SHA1='0bd35ea555b9aabaf30d255f3cb90aedf6bebca1'
SERVICE_NAME='airports-assembly'
SERVICE_VERSION='1.0.1'
docker build \
  --build-arg service_sha1=$SERVICE_SHA1 \
  --build-arg service_version=$SERVICE_VERSION \
  --build-arg service_name=$SERVICE_NAME \
  -t $SERVICE_NAME:$SERVICE_VERSION \
  java_service_base/

```

# My test

In my test I created a repository lunatechvaret on hub.docker.io and uploaded the images under this.
Therefore i used a prefix lunatechvaret/ on docker build command
